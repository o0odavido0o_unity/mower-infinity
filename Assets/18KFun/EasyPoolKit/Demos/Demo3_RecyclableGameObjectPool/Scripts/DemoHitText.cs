using UnityEngine;

namespace EasyPoolKit.Demo
{
    public class DemoHitText : RecyclableMonoBehaviour
    {
        public TextMesh Text;
    }
}