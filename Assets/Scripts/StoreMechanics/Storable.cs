using DG.Tweening;
using UnityEngine;



// script for GameObjects that can be stored
public class Storable : MonoBehaviour
{
    [SerializeField] private float storeAnimationDuration = 0.3f;
    [SerializeField] private float storeAnimationJumpPower = 3.0f;
    [SerializeField] private Ease storeAnimationEase;
    
    public Vector3 size { get; private set; }
    [SerializeField] private float storeScale = 0.5f;

    public enum StorableStates
    {
        Idle,
        Stored
    }
    public StorableStates curState { get; private set; }

    private Transform thisTransform;



    void Awake()
    {
        size = storeScale * GetComponent<MeshRenderer>().bounds.size;
        thisTransform = transform;
        curState = StorableStates.Idle;
    }

    public void Store(Vector3 newPosition, Transform newParent)
    {
        curState = StorableStates.Stored;

        // animation
        thisTransform.SetParent(newParent, true);
        thisTransform.DOLocalJump(newPosition, storeAnimationJumpPower, 1, storeAnimationDuration)
            .SetEase(storeAnimationEase);
        thisTransform.DOLocalRotate(new Vector3(0, 0, 0), storeAnimationDuration);
        thisTransform.DOScale(size, storeAnimationDuration);
    }
}