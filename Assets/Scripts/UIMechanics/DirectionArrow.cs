using UnityEngine;



public class DirectionArrow : MonoBehaviour
{
    [SerializeField] private Transform directionObject;
    private Transform thisTransform;



    private void Awake()
    {
        thisTransform = transform;
    }

    private void Update()
    {
        thisTransform.LookAt(directionObject);
    }
}