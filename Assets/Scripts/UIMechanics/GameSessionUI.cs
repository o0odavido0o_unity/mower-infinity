using UnityEngine;
using UnityEngine.UI;



public class GameSessionUI : MonoBehaviour
{
    [SerializeField] private Slider mainGrassAmountProgressIndicator;
    [SerializeField] private GameObject directionArrow;

    [SerializeField] private GameSessionUIAnimator thisAnimator;



    public void OnPlayerGrassAmountChange(float grassAmount)
    {
        mainGrassAmountProgressIndicator.value = grassAmount;

        if (grassAmount == 1f)
            OnPlayerGrassFull();

        if (grassAmount == 0f)
            OnPlayerGrassEmpty();
    }

    private void OnPlayerGrassFull()
    {
        thisAnimator.PlayFullGrassTxtAnim();
        directionArrow.SetActive(true);
    }

    private void OnPlayerGrassEmpty()
    {
        directionArrow.SetActive(false);
    }
}