using System;
using System.Threading.Tasks;
using UnityEngine;



public class CoinManager : Singleton<CoinManager>
{
    private int playerCoins = 0;
    public Action<int, Vector3> onAddCoinsFromWorld;
    public Action<int, Vector3> onAddCoinsFromUI;



    public void AddCoins(int coinAmount, Transform coinSource)
    {
        playerCoins += coinAmount;
        onAddCoinsFromWorld?.Invoke(playerCoins, coinSource.position);
    }

    public async void AddCoinsWithSpeed(int coinAmount, RectTransform coinSource, int coinSpeed)
    {
        int addingNum = coinAmount / coinSpeed;
        int lastAdd = coinAmount % coinSpeed;

        for (var count = 0; count < addingNum; count++)
        {
            playerCoins += coinSpeed;
            onAddCoinsFromUI?.Invoke(playerCoins, coinSource.position);

            await Task.Delay(10);
        }
        playerCoins += lastAdd;
        onAddCoinsFromUI?.Invoke(playerCoins, coinSource.position);
    }
}