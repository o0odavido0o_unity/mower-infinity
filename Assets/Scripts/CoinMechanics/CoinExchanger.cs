using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CoinExchanger : MonoBehaviour
{
    [SerializeField] private float betweenExchangeTime = 0.01f;
    
    
    
    void Start()
    {
        onExchangeComplete += OnExchangeComplete;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        var coinExchangeableStorer = other.GetComponent<CoinExchangeableStorer>();
        if (coinExchangeableStorer != null)
        {
            StartCoroutine(Exchanging(coinExchangeableStorer.ExchangeAll()));
        }
    }
    
    private IEnumerator Exchanging(List<CoinExchangeable> coinExchangeables)
    {
        var exchangerTransform = transform;
        foreach (var coinExchangeable in coinExchangeables)
        {
            coinExchangeable.Exchange(exchangerTransform, onExchangeComplete);
            yield return new WaitForSeconds(betweenExchangeTime);
        }

        coinExchangeables.Clear();
    }
    
    private Action<int> onExchangeComplete;
    private void OnExchangeComplete(int coins)
    {
        CoinManager.Instance.AddCoins(coins, transform);
    }
}