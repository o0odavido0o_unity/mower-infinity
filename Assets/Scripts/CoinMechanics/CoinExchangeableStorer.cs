using System;
using System.Collections.Generic;
using UnityEngine;



public class CoinExchangeableStorer : MonoBehaviour
{
    [SerializeField] private int storeHeight;
    [SerializeField] private int storeWidth;
    [SerializeField] private int storeLength;
    private List<List<StorerCell<Storable>>> cells;
    private int[] curCellCoord;
    
    private List<CoinExchangeable> coinExchangeables = new List<CoinExchangeable>();

    public Action<float> onAmountChanged;



    void Awake()
    {
        // initializing of storer
        cells = new List<List<StorerCell<Storable>>>();
        var positioningErrorInTheMiddleOfTheWidth = (float) ((storeWidth + 1) % 2) / 2;
        for (int floorNum = 0, heightRectCoord = 0; floorNum < storeHeight; floorNum++, heightRectCoord++)
        {
            cells.Add(new List<StorerCell<Storable>>());
            for (float cellNum1 = 0, widthRectCoord = -storeWidth / 2 + positioningErrorInTheMiddleOfTheWidth; cellNum1 < storeWidth; cellNum1++, widthRectCoord++)
            {
                for (int cellNum2 = 0, lengthRectCoord = -storeLength / 2;
                    cellNum2 < storeLength;
                    cellNum2++, lengthRectCoord++)
                {
                    cells[floorNum].Add(new StorerCell<Storable>
                    {
                        rectPosition = new[] {widthRectCoord, heightRectCoord, lengthRectCoord}
                    });
                }
            }
        }
        curCellCoord = new[] {0, 0};
    }

    public void Store(Collider other)
    {
        var storable = other.GetComponent<Storable>();
        if (storable is null)
            return;
        
        if(storable.curState == Storable.StorableStates.Stored)
            return;

        if (curCellCoord[0] >= cells.Count)
            return;

        // getting new position for storable from storer
        var storablePos = new Vector3(storable.size.x * cells[curCellCoord[0]][curCellCoord[1]].rectPosition[0],
            storable.size.y * cells[curCellCoord[0]][curCellCoord[1]].rectPosition[1],
            storable.size.z * cells[curCellCoord[0]][curCellCoord[1]].rectPosition[2]);
        curCellCoord[1]++;
        if (curCellCoord[1] >= cells[0].Count)
        {
            curCellCoord[1] = 0;
            curCellCoord[0]++;
        }

        coinExchangeables.Add(storable.GetComponent<CoinExchangeable>());
        storable.Store(storablePos, transform);

        onAmountChanged((float) (curCellCoord[0] * storeWidth * storeLength + curCellCoord[1]) / (storeWidth * storeLength * storeHeight));
    }

    public List<CoinExchangeable> ExchangeAll()
    {
        // clear storer
        curCellCoord = new[] { 0, 0 };

        onAmountChanged(0);

        var coinExchangeablesCopy = new List<CoinExchangeable>(coinExchangeables);
        coinExchangeables.Clear();
        return coinExchangeablesCopy;
    }
}