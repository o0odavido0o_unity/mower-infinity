using System;
using DG.Tweening;
using EasyPoolKit;
using UnityEngine;



// script for GameObjects that can be exchanged for coins
public class CoinExchangeable : MonoBehaviour
{
    // value of object in coins
    [SerializeField] private int value;
    
    [Header("Animation")]
    [SerializeField] private float exchangeAnimationDuration = 1f;
    [SerializeField] private float exchangeAnimationJumpPower = 3.0f;
    [SerializeField] private Ease exchangeAnimationEase = Ease.Linear;
    
    private Transform thisTransform;
    
    
    
    private void Awake()
    {
        thisTransform = transform;
    }

    public void Exchange(Transform exchangerTransform, Action<int> onExchangeComplete)
    {
        // animation
        thisTransform.SetParent(exchangerTransform, true);
        thisTransform.DOJump(exchangerTransform.position, exchangeAnimationJumpPower, 1, exchangeAnimationDuration)
            .SetEase(exchangeAnimationEase)
            .OnComplete(() =>
            {
                onExchangeComplete(value);
                GetComponent<RecyclableMonoBehaviour>().DespawnSelf();
            });
    }
}