using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSessionUIAnimator : MonoBehaviour
{
    [SerializeField] private Animator fullGrassTxtAnimator;



    public void PlayFullGrassTxtAnim()
    {
        fullGrassTxtAnimator.SetTrigger("FullGrass");
    }
}