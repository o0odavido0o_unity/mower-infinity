using System;
using UnityEngine;



public class HumanAnimator : MonoBehaviour
{
    [SerializeField] private Animator humanAnimator;

    public Action onHarvestDamageActivate;
    public Action onHarvestDamageDeactivate;



    public void ActivateWalk()
    {
        humanAnimator.SetBool("Walk", true);
    }

    public void ActivateRun()
    {
        humanAnimator.SetBool("Run", true);
    }

    public void ActivateHarvest()
    {
        humanAnimator.SetBool("Harvest", true);
    }

    public void ActivateIdle()
    {
        humanAnimator.SetBool("Walk", false);
        humanAnimator.SetBool("Run", false);
        humanAnimator.SetBool("Harvest", false);
    }

    public void ActivateHarvestDamage()
    {
        onHarvestDamageActivate?.Invoke();
    }

    public void DeactivateHarvestDamage()
    {
        onHarvestDamageDeactivate?.Invoke();
    }
}