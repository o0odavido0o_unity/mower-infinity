using UnityEngine;



public class IsometricMover : MonoBehaviour
{
    [SerializeField] private CharacterController thisCharacterController;
    private float turnSmoothVelocity;



    public void Move(Vector2 inputForMoving, Movement desirableMovement)
    {
        var directionToMoveTo = new Vector3(inputForMoving.x, 0f, inputForMoving.y).normalized;

        // computes and applies angle to rotate to
        var desirableAngle = Mathf.Atan2(directionToMoveTo.x, directionToMoveTo.z) * Mathf.Rad2Deg;
        var angleToRotateTo = Mathf.SmoothDampAngle(transform.eulerAngles.y, desirableAngle, ref turnSmoothVelocity,
            desirableMovement.turnSmoothTime);
        transform.rotation = Quaternion.Euler(0f, angleToRotateTo, 0f);

        thisCharacterController.Move(directionToMoveTo * desirableMovement.speed * Time.deltaTime);
    }
}

public class Movement
{
    public float speed { set; get; }
    public float turnSmoothTime { set; get; }
}