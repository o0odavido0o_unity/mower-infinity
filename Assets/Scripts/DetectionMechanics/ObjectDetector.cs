using System;
using UnityEngine;



public class ObjectDetector : MonoBehaviour
{
    public Action<Collider> onTriggerEnter;
    private void OnTriggerEnter(Collider obj)
    {
        onTriggerEnter?.Invoke(obj);
    }

    public Action<Collider> onTriggerStay;
    private void OnTriggerStay(Collider obj)
    {
        onTriggerStay?.Invoke(obj);
    }
}