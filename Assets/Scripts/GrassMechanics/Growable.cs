using System;
using UnityEngine;



public class Growable : MonoBehaviour
{
    [SerializeField] private float timeToGrow = 10f;
    private float growTimer;
    
    [SerializeField] private GameObject modelToGrow;
    private Transform modelToGrowTransform;
    [SerializeField] private float startGrowScale = 1f;
    [SerializeField] private float endGrowScale = 6f;
    private Vector3 growScaleChangeSpeed;

    private enum GrowableStates
    {
        Idle,
        Grow
    }
    private GrowableStates growableState = GrowableStates.Idle;



    void Start()
    {
        modelToGrowTransform = modelToGrow.transform;
        growScaleChangeSpeed = new Vector3(0f, (endGrowScale - startGrowScale) / timeToGrow, 0f);
    }
    
    public Action onGrow;
    void Update()
    {
        if (growableState == GrowableStates.Grow)
        {
            growTimer += Time.deltaTime;
            modelToGrowTransform.localScale += growScaleChangeSpeed * Time.deltaTime;
            if (growTimer > timeToGrow)
            {
                growableState = GrowableStates.Idle;
                modelToGrow.SetActive(false);
                onGrow?.Invoke();
            }
        }
    }

    public void StartGrowing()
    {
        growTimer = 0f;
        growableState = GrowableStates.Grow;
        modelToGrow.SetActive(true);
        modelToGrowTransform.localScale = new Vector3(modelToGrowTransform.localScale.x, startGrowScale,
            modelToGrowTransform.localScale.z);
    }
}