using System;
using UnityEngine;



public class Grass : MonoBehaviour
{
    [SerializeField] private Harvestable harvestable;
    [SerializeField] private Dropper dropper;
    [SerializeField] private Growable growable;



    private void Start()
    {
        harvestable.onHarvest += OnHarvest;
        growable.onGrow += OnGrow;
    }

    private void OnHarvest(Action<MonoBehaviour> onDropItemAfterHarvest)
    {
        dropper.Drop(onDropItemAfterHarvest);
        growable.StartGrowing();
    }

    private void OnGrow()
    {
        harvestable.GrowObject();
    }
}