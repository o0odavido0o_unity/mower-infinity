using DG.Tweening;
using TMPro;
using UnityEngine;



public class UIManager : Singleton<UIManager>
{
    [SerializeField] private GameSessionUI gameSessionUI;
    [SerializeField] private RectTransform mainCanvas;
    
    [SerializeField] private TextMeshProUGUI mainCoinAmountTxt;
    [SerializeField] private RectTransform mainCoinAmountImg;
    [SerializeField] private RectTransform coinPref;
    [SerializeField] private float addingCoinAnimationDuration = 1f;
    
    
    
    public void AddCoinsFromWorld(int mainCoinAmount, Vector3 coinSourceWorld)
    {
        var coinSourceUI = Camera.main.WorldToScreenPoint(coinSourceWorld);
        AddCoinsFromUI(mainCoinAmount, coinSourceUI);
    }

    public void AddCoinsFromUI(int mainCoinAmount, Vector3 coinSourceUI)
    {
        mainCoinAmountTxt.text = mainCoinAmount.ToString();

        var coin = SpawnManager.Instance.SpawnCoin();
        var coinTransform = coin.GetComponent<RectTransform>();
        coinTransform.SetParent(mainCanvas, false);
        coinTransform.position = coinSourceUI;
        coinTransform.DOMove(mainCoinAmountImg.position, addingCoinAnimationDuration)
            .OnComplete(() =>
            {
                coin.DespawnSelf();
            });
    }

    public void ChangePlayerGrassAmount(float grassAmount)
    {
        gameSessionUI.OnPlayerGrassAmountChange(grassAmount);
    }
}