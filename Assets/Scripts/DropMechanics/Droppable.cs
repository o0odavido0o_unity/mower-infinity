using System;
using DG.Tweening;
using UnityEngine;



public class Droppable : MonoBehaviour
{
    [SerializeField] private float dropAnimationDuration = 0.3f;
    [SerializeField] private float dropAnimationJumpPower = 3.0f;
    [SerializeField] private Ease dropAnimationEase = Ease.Unset;
    
    
    
    public void Drop(Action<MonoBehaviour> OnDropItemAfterHarvest)
    {
        // animation
        transform.DOJump(transform.position, dropAnimationJumpPower, 1, dropAnimationDuration)
            .SetEase(dropAnimationEase)
            .OnComplete(() =>
            {
                OnDropItemAfterHarvest?.Invoke(this);
            });
    }
}