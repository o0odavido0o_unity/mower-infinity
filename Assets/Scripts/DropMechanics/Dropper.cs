using System;
using UnityEngine;



public class Dropper : MonoBehaviour
{
    [SerializeField] private Droppable droppablePref;



    public void Drop(Action<MonoBehaviour> onDropItemAfterHarvest)
    {
        var droppable = SpawnManager.Instance.SpawnGrassBlock().GetComponent<Droppable>();
        droppable.transform.SetParent(transform, false);
        droppable.Drop(onDropItemAfterHarvest);
    }
}