using UnityEngine;
using UnityEngine.EventSystems;



// detects input for floating joystick
public class CustomFloatingJoystickArea : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private RectTransform areaRectTransform;
    [SerializeField] private CustomFloatingJoystick joystick;
    [SerializeField] private RectTransform joystickRectTransform;



    private void Update()
    {
#if UNITY_EDITOR
        // for computer input detection is implemented next way:
        // on pointer down activates joystick and sends pointer position
        // on pointer up deactivates joystick
        // implemented in strange way, because IPointerUpHandler didn't work

        if (Input.GetMouseButtonUp(0))
        {
            OnPointerUp();
        }
#elif UNITY_ANDROID
        // for android input detection is implemented next way:
        // only first touch activates joystick and sends position
        // when there is no touch, deactivates joystick

        var isThereTouches = Input.touchCount > 0;
        if (isThereTouches)
        {
            var firstTouch = Input.GetTouch(0);
            var isFirstTouchInJoystickArea = RectTransformUtility.RectangleContainsScreenPoint(areaRectTransform, firstTouch.position);
            var isJoystickInItsArea = joystickRectTransform.gameObject.activeSelf &&
                RectTransformUtility.RectangleContainsScreenPoint(areaRectTransform, joystickRectTransform.position);
            if (isFirstTouchInJoystickArea || isJoystickInItsArea)
                joystick.ActivateJoystick(firstTouch.position);
        }
        else
            joystick.DeactivateJoystick();
#endif
    }

    private void OnPointerUp()
    {
        joystick.DeactivateJoystick();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        joystick.ActivateJoystick(Input.mousePosition);
    }
}