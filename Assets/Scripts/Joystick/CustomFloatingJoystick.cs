using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.OnScreen;



public class CustomFloatingJoystick : MonoBehaviour
{
    [SerializeField] private OnScreenStick stick;
    private bool isActive = false;

    

    // activates joystick and sets it position
    public void ActivateJoystick(Vector3 position)
    {
        var dragEventData = new PointerEventData(EventSystem.current);
        dragEventData.position = position;
        ExecuteEvents.Execute(stick.gameObject, dragEventData, ExecuteEvents.dragHandler);

        if (isActive)
            return;

        gameObject.GetComponent<RectTransform>().position = position;
        gameObject.SetActive(true);
        isActive = true;
    }

    public void DeactivateJoystick()
    {
        if (!isActive)
            return;

        isActive = false;
        gameObject.SetActive(false);
    }
}