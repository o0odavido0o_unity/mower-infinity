using System;
using UnityEngine;
using UnityEngine.Advertisements;



public class AdsManager : Singleton<AdsManager>, IUnityAdsInitializationListener
{
    [SerializeField] private string _androidGameId = "5005661";
    [SerializeField] private string _iOSGameId = "5005660";
    [SerializeField] private bool _testMode = true;
    private string _gameId;



    void Awake()
    {
        InitializeAds();
    }

    public void InitializeAds()
    {
        _gameId = (Application.platform == RuntimePlatform.IPhonePlayer)
            ? _iOSGameId
            : _androidGameId;
        Advertisement.Initialize(_gameId, _testMode, true, this);
    }

    public Action onInitializeComplete;
    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
        onInitializeComplete?.Invoke();
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
}