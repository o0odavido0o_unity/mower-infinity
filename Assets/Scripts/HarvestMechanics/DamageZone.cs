using System;
using UnityEngine;



public class DamageZone : MonoBehaviour
{
    [SerializeField] private MeshCollider damageZone;



    public Action<Collider> onTriggerEnter;
    private void OnTriggerEnter(Collider obj)
    {
        onTriggerEnter?.Invoke(obj);
    }

    public void Activate()
    {
        damageZone.enabled = true;
    }

    public void Deactivate()
    {
        damageZone.enabled = false;
    }
}