using System;
using UnityEngine;



// script for GameObjects that can be harvested
public class Harvestable : MonoBehaviour
{ 
    private void HarvestObject()
    {
        gameObject.SetActive(false);
    }

    public void GrowObject()
    {
        gameObject.SetActive(true);
    }

    public Action<Action<MonoBehaviour>> onHarvest;
    public void Harvest(Action<MonoBehaviour> onDropItemFromHarvest)
    {
        HarvestObject();

        onHarvest?.Invoke(onDropItemFromHarvest);
    }
}