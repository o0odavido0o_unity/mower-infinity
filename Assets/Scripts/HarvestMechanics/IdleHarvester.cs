using System;
using System.Collections;
using UnityEngine;



// class that implements idle harvesting mechanic
public class IdleHarvester : MonoBehaviour
{
    // handles detection of an objects that can be harvested
    public DamageZone harvestDamageZone;
    [SerializeField] private float requiredHarvestAfterTime = 0.3f;
    // determines if idleharvester can harvest now
    private bool? canHarvest;
    public Action onHarvestingStart;
    public Action onHarvestingEnd;
    
    
    
    // starts process of harvesting if object to harvest is detected
    public void Harvest(Collider obj)
    {
        var harvestable = obj.GetComponent<Harvestable>();
        if (harvestable is null)
            return;

        if (canHarvest == null)
            StartCoroutine(Harvesting());
        else
            canHarvest = true;
    }
    
    // process of harvesting
    private IEnumerator Harvesting()
    {
        onHarvestingStart?.Invoke();
        canHarvest = true;

        while (canHarvest == true)
        {
            canHarvest = false;
            yield return new WaitForSeconds(requiredHarvestAfterTime);
        }

        canHarvest = null;
        onHarvestingEnd?.Invoke();
    }
}