using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;



public class Player : MonoBehaviour
{
    [Header("Input")] 
    [SerializeField] private PlayerInput playerInput;

    [Header("Mechanics")]
    [SerializeField] private IsometricMover isometricMover;
    [SerializeField] private IdleHarvester idleHarvester;
    [SerializeField] private CoinExchangeableStorer coinExchangeableStorer;

    // detect objects
    [Header("Object Detectors")]
    [SerializeField] private ObjectDetector objectDetector;

    [Header("Animation and Effects")] 
    [SerializeField] private HumanAnimator humanAnimator;
    [SerializeField] private VisualEffect swingEffect;
    [SerializeField] private bool playSwingEffect = true;

    [Header("Characteristics")] 
    [SerializeField] private float walkSpeed = 3.0f;
    [SerializeField] private float runSpeed = 6.0f;
    [SerializeField] private float harvestMoveSpeed = 3.0f;
    [SerializeField] private float turnSmoothTime = 0.1f;
    private Movement thisMovement;

    private enum PlayerStates
    {
        Idle = 0,
        Walk = 1,
        Run = 2,
        Harvest = 3
    }
    private PlayerStates playerState;



    void Start()
    {
        thisMovement = new Movement
        {
            speed = walkSpeed,
            turnSmoothTime = turnSmoothTime,
        };

        objectDetector.onTriggerEnter += OnObjectDetectorTriggerEnter;
        objectDetector.onTriggerStay += OnObjectDetectorTriggerStay;
        idleHarvester.onHarvestingStart += onHarvestingStart;
        idleHarvester.onHarvestingEnd += onHarvestingEnd;
        idleHarvester.harvestDamageZone.onTriggerEnter += OnHarvestDamageZoneTriggerEnter;
        humanAnimator.onHarvestDamageActivate += OnHarvestDamageActivate;
        humanAnimator.onHarvestDamageDeactivate += OnHarvestDamageDeactivate;
    }

    void Update()
    {
        // controls player
        var inputForMoving = playerInput.actions["Move"].ReadValue<Vector2>();
        if (inputForMoving.magnitude >= 0.9f)
        {
            ChangePlayerState(PlayerStates.Run);
            isometricMover.Move(inputForMoving, thisMovement);
        }
        else if (inputForMoving.magnitude >= 0.1f)
        {
            ChangePlayerState(PlayerStates.Walk);
            isometricMover.Move(inputForMoving, thisMovement);
        }
        else
        {
            ChangePlayerState(PlayerStates.Idle);
        }
    }

    private void ChangePlayerState(PlayerStates state, bool isFromHarvest = false)
    {
        // if player state is the same then there is no need to change anything
        // if state is Harvest and player state is being tried to be changed not from harvest logic
        // then changes won't be applied because Harvest state is prioritized over other states
        if (state == playerState || (playerState == PlayerStates.Harvest && !isFromHarvest))
            return;

        playerState = state;
        switch (playerState)
        {
            case PlayerStates.Idle:
                humanAnimator.ActivateIdle();
                break;
            case PlayerStates.Walk:
                humanAnimator.ActivateWalk();
                thisMovement.speed = walkSpeed;
                break;
            case PlayerStates.Run:
                humanAnimator.ActivateRun();
                thisMovement.speed = runSpeed;
                break;
            case PlayerStates.Harvest:
                humanAnimator.ActivateHarvest();
                thisMovement.speed = harvestMoveSpeed;
                break;
        }
    }

    private void OnObjectDetectorTriggerEnter(Collider obj)
    {
        idleHarvester.Harvest(obj);
        coinExchangeableStorer.Store(obj);
    }

    private void OnObjectDetectorTriggerStay(Collider obj)
    {
        idleHarvester.Harvest(obj);
    }

    private void onHarvestingStart()
    {
        ChangePlayerState(PlayerStates.Harvest);
    }

    private void onHarvestingEnd()
    {
        ChangePlayerState(PlayerStates.Idle, true);
    }

    private void OnHarvestDamageZoneTriggerEnter(Collider obj)
    {
        var harvestable = obj.GetComponent<Harvestable>();
        if (harvestable is null)
            return;

        harvestable.Harvest(OnDropItemFromHarvest);
    }

    private void OnDropItemFromHarvest(MonoBehaviour obj)
    {
        var storable = obj.GetComponent<Collider>();
        if(storable is null)
            return;

        coinExchangeableStorer.Store(storable);
    }

    private void OnHarvestDamageActivate()
    {
        idleHarvester.harvestDamageZone.Activate();
        if (playSwingEffect)
            swingEffect.Play();
    }
    private void OnHarvestDamageDeactivate()
    {
        idleHarvester.harvestDamageZone.Deactivate();
    }
}