using UnityEngine;



public class MainManager : MonoBehaviour
{
    private UIManager uiManager;
    private CoinManager coinManager;

    [SerializeField] private CoinExchangeableStorer playerStorer;



    void Awake()
    {
        uiManager = UIManager.Instance;
        coinManager = CoinManager.Instance;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        coinManager.onAddCoinsFromWorld += uiManager.AddCoinsFromWorld;
        coinManager.onAddCoinsFromUI += uiManager.AddCoinsFromUI;
        playerStorer.onAmountChanged += uiManager.ChangePlayerGrassAmount;
    }
}