using EasyPoolKit;
using UnityEngine;



public class SpawnManager : Singleton<SpawnManager>
{
    [SerializeField] private GameObject coinPref;
    [SerializeField] private GameObject grassBlockPref;



    private void Start()
    {
        Init();
    }

    private void Init()
    {
        var grassBlockPoolConfig = new RecyclablePoolConfig
        {
            ObjectType = RecycleObjectType.RecyclableGameObject,
            ReferenceType = typeof(RecyclableMonoBehaviour),
            PoolId = "grassBlockPool",
            InitCreateCount = 100,
            ClearType = PoolClearType.ClearToLimit,
            IfIgnoreTimeScale = false,
        };
        RecyclableGOPoolKit.Instance.RegisterPrefab(grassBlockPref, grassBlockPoolConfig);

        var coinPoolConfig = new RecyclablePoolConfig
        {
            ObjectType = RecycleObjectType.RecyclableGameObject,
            ReferenceType = typeof(RecyclableMonoBehaviour),
            PoolId = "CoinPool",
            InitCreateCount = 100,
            ClearType = PoolClearType.ClearToLimit,
            IfIgnoreTimeScale = false,
        };
        RecyclableGOPoolKit.Instance.RegisterPrefab(coinPref, coinPoolConfig);
    }

    public RecyclableMonoBehaviour SpawnCoin()
    {
        var newCoin = RecyclableGOPoolKit.Instance.SimpleSpawn<RecyclableMonoBehaviour>(coinPref);
        return newCoin;
    }

    public RecyclableMonoBehaviour SpawnGrassBlock()
    {
        var grassBlock = RecyclableGOPoolKit.Instance.SimpleSpawn<RecyclableMonoBehaviour>(grassBlockPref);
        return grassBlock;
    }
}